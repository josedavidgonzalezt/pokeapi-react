export const GET_LIST_POKEMON = "GET_LIST_POKEMON";
export const GET_LIST_POKEMON_SUCCESSS = "GET_LIST_POKEMON_SUCCESSS";
export const GET_LIST_POKEMON_ERROR = "GET_LIST_POKEMON_ERROR";

export const GET_POKEMON = "GET_POKEMON";
export const GET_POKEMON_SUCCESS = "GET_POKEMON_SUCCESS";
export const GET_POKEMON_ERROR = "GET_POKEMON_ERROR";