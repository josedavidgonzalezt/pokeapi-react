import axios from "axios";
import { useReducer } from "react";
import {
  GET_LIST_POKEMON,
  GET_LIST_POKEMON_SUCCESSS,
  GET_LIST_POKEMON_ERROR,
  GET_POKEMON_SUCCESS,
  GET_POKEMON,
  GET_POKEMON_ERROR,
} from "../types";
import PokemonContext from "./pokemonContex";
import pokemonReducer from "./pokemonReducer";

const PokemonState = ({ children }) => {
  const initialState = {
    listPokemons: {},
    pokemon: [],
    pokemon_actual: null,
    loading: false,
  };
  const [state, dispatch] = useReducer(pokemonReducer, initialState);

  const getListPokemons = async (numero) => {
    dispatch({ type: GET_LIST_POKEMON });
    try {
      const datos = await axios(
        `https://pokeapi.co/api/v2/pokemon?offset=${numero}&limit=5`
      );
      const datos2 = [];
      for (let i = 0; i < datos.data.results.length; i++) {
        const respuesta = await axios(datos.data.results[i].url);
        datos2.push(respuesta.data);
      }

      dispatch({
        payload: {
          datos: datos.data,
          datos2: datos2,
        },
        type: GET_LIST_POKEMON_SUCCESSS,
      });
    } catch (error) {
      console.log(error);
      dispatch({ type: GET_LIST_POKEMON_ERROR });
    }
  };

  const getPokemonActual = async (nombre) => {
    dispatch({ type: GET_POKEMON });
    try {
      const getNormal = axios.get(
        `https://pokeapi.co/api/v2/pokemon/${nombre.trim()}`
      );
      const getSpecies = axios.get(
        `https://pokeapi.co/api/v2/pokemon-species/${nombre.trim()}`
      );

      const [normal, species] = await Promise.all([getNormal, getSpecies]);
      

      const datos = {
        data: {
          name: normal.data.name,
          experience: normal.data.base_experience,
          ability: normal.data.abilities,
          order: normal.data.id,
          text_entries: species.data.flavor_text_entries.filter(f=>f.language.name==="es"),
          type: species.data.egg_groups,
          color: species.data.color.name,
        },
      };
      
      dispatch({
        payload: datos.data,
        type: GET_POKEMON_SUCCESS,
      });
    } catch (error) {
      console.log(error);
      dispatch({ type: GET_POKEMON_ERROR });
    }
  };

  return (
    <PokemonContext.Provider
      value={{
        listPokemons: state.listPokemons,
        pokemon: state.pokemon,
        pokemon_actual: state.pokemon_actual,
        loading: state.loading,
        getListPokemons: getListPokemons,
        getPokemonActual: getPokemonActual,
      }}
    >
      {children}
    </PokemonContext.Provider>
  );
};

export default PokemonState;
