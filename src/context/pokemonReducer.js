import { 
    GET_LIST_POKEMON,
    GET_LIST_POKEMON_SUCCESSS,
    GET_LIST_POKEMON_ERROR,
    GET_POKEMON_SUCCESS,
    GET_POKEMON,
    GET_POKEMON_ERROR,
 } from "../types";

const pokemonReducer =(state, action) => {
    switch (action.type) {
        case GET_POKEMON:
       case GET_LIST_POKEMON:
           return{
               ...state,
               loading: true,
               pokemon_actual:null
           }
       case GET_LIST_POKEMON_SUCCESSS:
           return{
               ...state,
               listPokemons: action.payload.datos,
               pokemon: action.payload.datos2,
               loading: false
           }
       case GET_LIST_POKEMON_ERROR:
       case GET_POKEMON_ERROR:
           return{
               ...state,
               loading: false
           }
       case GET_POKEMON_SUCCESS:
           return{
               ...state,
               pokemon_actual: action.payload, 
               loading:false
           } 
        default:
           return state
    }
}

export default pokemonReducer;