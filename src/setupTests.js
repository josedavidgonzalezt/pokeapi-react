const Enzyme = require('enzyme')
const Adapter = require('@wojtekmaj/enzyme-adapter-react-17')

// Configure Enzyme with React 16 adapter
Enzyme.configure({ adapter: new Adapter() })
