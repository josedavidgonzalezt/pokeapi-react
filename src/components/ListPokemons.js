import { useContext, useEffect, useState } from "react";
import Pokemon from "./Pokemon";
import PokemonContext from "../context/pokemonContex";

const ListPokemons = () => {
  const pokemonContext = useContext(PokemonContext);
  const { getListPokemons, listPokemons, loading, pokemon } = pokemonContext;
  const [state, setstate] = useState(0);

  const actButton = (valor) => {
    if (state < valor) {
      setstate(valor);
    } else if(state > valor){
      setstate(valor);
    }
    if(state === 0 && valor === -5){
      setstate(state);
    }
  };
  useEffect(() => {
    getListPokemons(state);
    // eslint-disable-next-line
  }, [state]);

  return (
    <>
      <div className="contenedor">
        <div className="after-before">
          <button className="btn" onClick={() => actButton(state - 5)}>Anterior</button>
          <button className="btn" onClick={() => actButton(state + 5)}>Siguiente</button>
        </div>
        {loading
          ?<div className="lds-ellipsis"><div></div><div></div><div></div><div></div></div>
          :<>
            <div className="contenedor-cards">
            {Object.keys(listPokemons).length !== 0
              ? pokemon.map((pok) => <Pokemon key={pok.id} pok={pok} />)
              : null}
            </div>
          </>
        }
      </div>
    </>
  );
};

export default ListPokemons;
