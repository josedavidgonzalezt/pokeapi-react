import React, { useContext, useEffect } from "react";
import { useRouteMatch } from "react-router-dom";
import PokemonContext from "../context/pokemonContex";

const PokemonDetails = () => {
  const pokemonContext = useContext(PokemonContext);
  const { getPokemonActual, pokemon_actual, loading } = pokemonContext;

  const router = useRouteMatch();
  const { params } = router;

  useEffect(() => {
      const prueba = ()=>{
        if (params) {
            getPokemonActual(params.nombre);
          }
      }
      prueba()
      // eslint-disable-next-line
  }, []);
  
  
  return (
    <div className="container-details">
        {(!pokemon_actual || loading)
            ?<div className="lds-ellipsis"><div></div><div></div><div></div><div></div></div>
            : <div className="card-details">
            <div className="img-details" style={{backgroundImage: `url(https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/official-artwork/${pokemon_actual.order}.png)`}}></div>
            <div className="cuerpo-details">
                <p>{pokemon_actual.name}</p>
                <p>Tipo: {pokemon_actual.type.map(p=> (`${p.length >0 ?p.name :p.name + " |"} `))}</p>
                <p>Experience: {pokemon_actual.experience}</p>
                <ul>Habilidades: {pokemon_actual.ability.map(a=>(
                    <li key={a.ability.name}>{a.ability.name}</li>
                ))}
                </ul>
                <p>Descripción: {pokemon_actual.text_entries[0].flavor_text}</p>
            </div>
         </div>
        }
       
    </div>
  );
};

export default PokemonDetails;
