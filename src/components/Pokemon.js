import { Link } from "react-router-dom";

const Pokemon = ({ pok }) => {

    return (
      
       <div className="card">
          <div className="img-card" style={{backgroundImage: `url(https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/official-artwork/${pok.id}.png)`}}>
          </div>
          <p>{pok.name}</p>
          <Link to={`/pokemon/${pok.name}`}>
            <p className="ver mas">ver mas</p>
          </Link> 
       </div>
      
    );
}
 
export default Pokemon;