import ListPokemons from "./components/ListPokemons";
import Header from './components/Header';
import {BrowserRouter as Router, Route, Switch} from 'react-router-dom';
import PokemonDetails from "./components/PokemonDetails";


function App() {

  return (
    <>
     <Router>
       <Header/>
       <Switch>
         <Route exact path="/" component={ListPokemons}/>
         <Route exact path="/pokemon/:nombre" component={PokemonDetails} />
       </Switch>
     </Router>
    </>
  );
}

export default App;
